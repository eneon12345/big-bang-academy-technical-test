# Section 1: (20 mins)

1. What is the purpose of the "key" prop in React?
   The purpose of the key prop is to identify each element in a list and let React know which one has been updated. This is to improve rendering as React reconciles the difference between virtual DOM and real DOM. The key should also be unique.

2. What is the purpose of the "useEffect" hook in React?
   The purpose of the useEffect hook is to perform business logic after React renders the component. It can also be used to perform side effects after changes to a list of variables (dependencies). UseEffect is similar to componentDidMount and componentWillUnmount lifecycles from class component.

3. What is the purpose of the "setState" function in React class components?
   The purpose of setState is to let React know that the component's internal states have been changed so that React will rerender the component after these changes. If rerendering is not the intended purpose, then instead of setting states inside the component, consider passing them as props or use useRef instead.

4. What technique is commonly used to handle authentication and authorization in Node.js?
   One common authentication is JWT. When a user logs in, NodeJS verifies the user's credentials, signs a JWT and returns it to the client. Then when the client tries to access protected APIs, NodeJS authenticates the JWT (ex: with JWT passport), and authorizes access once verified.

5. What is the role of a package manager in Node.js?
   The purpose of package manager (npm or yarn) is to manage dependencies of the project. It shows the libs or packages installed, their versions and their dependency relationship so that the project can be built efficiently.

# Section 2

# Init Server

1. cd server
2. yarn install
3. yarn build
4. add env file
5. yarn dev

# Init Client

1. cd client
2. yarn install
3. add env file
4. yarn start
